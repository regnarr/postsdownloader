package posts;

import org.junit.Test;
import posts.impl.JSONPostsFileWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PostsAppTest {

    @Test
    public void shouldParseResponseToRightListSize(){
        List<PostDto> postsDtoList = PostDto.createPostDtoListFromResponse(TestUtils.getJsonHardcodedPosts());
        assertEquals(postsDtoList.size(), 3);
    }

    @Test
    public void shouldProperlyParseFirstPostDtoFromPostsDtoList(){
        PostDto dto = new PostDto();
        dto.setId(1);
        dto.setUserId(1);
        dto.setTitle("sunt aut facere repellat provident occaecati excepturi optio reprehenderit");
        dto.setBody("quia et suscipit nsuscipit recusandae consequuntur expedita et cum nreprehenderit molestiae ut ut quas totam nnostrum rerum est autem sunt rem eveniet architecto");

        List<PostDto> postsDtoList = PostDto.createPostDtoListFromResponse(TestUtils.getJsonHardcodedPosts());

        assertEquals(dto, postsDtoList.get(0));
    }

    @Test
    public void shouldProperlyParseSecondPostDtoFromPostsDtoList(){
        PostDto dto = new PostDto();
        dto.setId(2);
        dto.setUserId(1);
        dto.setTitle("qui est esse");
        dto.setBody("est rerum tempore vitae nsequi sint nihil reprehenderit dolor beatae ea dolores neque nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis nqui aperiam non debitis possimus qui neque nisi nulla");

        List<PostDto> postsDtoList = PostDto.createPostDtoListFromResponse(TestUtils.getJsonHardcodedPosts());

        assertEquals(dto, postsDtoList.get(1));
    }

    @Test
    public void shouldSaveTwoPostsInTestingFolder() throws IOException {
        List<PostDto> testingPostDtoList = TestUtils.getTestingPostDtoList();
        JSONPostsFileWriter writer = new JSONPostsFileWriter();
        writer.write(testingPostDtoList);

        File file = new File("./testingFolder/1.json");
        File file2 = new File("./testingFolder/2.json");
        assertTrue(file.exists());
        assertTrue(file2.exists());
    }

    @Test
    public void shouldProperlySavePostJsonFileWithValidContent() throws IOException {
        List<PostDto> testingPostDtoList = TestUtils.getTestingPostDtoList();
        JSONPostsFileWriter writer = new JSONPostsFileWriter();
        writer.write(testingPostDtoList);

        File file = new File("./testingFolder/1.json");

        List<String> savedFile = Files.readAllLines(file.toPath());
        String expectedFileContent = "[{\"id\":1,\"userId\":1,\"title\":\"This is title of main post\",\"body\":\"Body of main post\"}]";

        assertEquals(expectedFileContent, savedFile.toString());
    }

}


