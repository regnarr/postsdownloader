package posts;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.Data;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Data
public class PostDto {

    private long id;
    private long userId;
    private String title;
    private String body;

    public static List<PostDto> createPostDtoListFromResponse(String responseBody){
       Type postListType = new TypeToken<ArrayList<PostDto>>(){}.getType();
       return new Gson().fromJson(responseBody, postListType);
    }

}
