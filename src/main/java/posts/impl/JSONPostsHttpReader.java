package posts.impl;

import posts.PostDto;
import posts.PostsReader;
import lombok.extern.slf4j.Slf4j;
import utils.AppProperties;
import utils.HttpConnection;

import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
public class JSONPostsHttpReader implements PostsReader {

    @Override
    public List<PostDto> read() {
        Optional<HttpResponse<String>> responseOptional = HttpConnection.getResponseFromURL(AppProperties.getProperty("postsApiUrl"));
        if (responseOptional.isPresent()){
            HttpResponse<String> response = responseOptional.get();
            System.out.println(response.body());
            return PostDto.createPostDtoListFromResponse(response.body());
        }else{
            return new ArrayList<>();
        }
    }
}
