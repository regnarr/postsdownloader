package posts.impl;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import posts.PostDto;
import posts.PostsWriter;
import utils.AppProperties;
import utils.StringUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
@Slf4j
public class JSONPostsFileWriter implements PostsWriter {
    private String dirPath;

     @Override
    public void write(List<PostDto> postDtoList) {
         dirPath = AppProperties.getProperty("postsFilePath");
         if(StringUtils.isEmpty(dirPath)){
             log.error("Can't load property postsFilePath");
             return;
         }
         makePostsDirectories();
         savePostsToFiles(postDtoList);
     }

    private void makePostsDirectories() {
        new File(dirPath).mkdirs();
    }

    private void savePostsToFiles(List<PostDto> postDtoList) {
        for (PostDto dto: postDtoList) {
           saveSinglePostToFile(dto);
        }
    }

    private void saveSinglePostToFile(PostDto dto){
        Gson gson = new Gson();
        Writer writer = null;
        try {
            String filePath = dirPath + "/"+ dto.getId() + ".json";
            writer =new FileWriter(filePath);
            gson.toJson(dto, writer);
            writer.flush();
            writer.close();
            log.info("Post id:{} successfully saved in {}", dto.getId(), filePath);
        } catch (IOException e) {
            log.error("Couldn't write post with id:{} to file", dto.getId(), e);
        }finally {
            closeWriter(writer);
        }
    }

    private void closeWriter(Writer writer) {
        if(writer!=null){
            try {
                writer.close();
            } catch (IOException e) {
                log.error("Can't close writer", e);
            }
        }
    }
}
