package posts;

import java.util.List;

public interface PostsWriter {

    void write(List<PostDto> postDtoList);
}
