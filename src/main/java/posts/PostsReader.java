package posts;

import java.util.List;

public interface PostsReader {

    List<PostDto> read();
}
