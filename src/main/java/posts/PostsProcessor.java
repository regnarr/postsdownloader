package posts;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PostsProcessor {

    private final PostsReader postsReader;
    private final PostsWriter postsWriter;

    public void start(){
        postsWriter.write(postsReader.read());
    }

}
