package utils;

import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@Slf4j
public class AppProperties {

    private static Properties appProps;

    private static void loadPropertiesFile() {
        String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String appConfigPath = rootPath + "app.properties";
        appProps = new Properties();
        try {
            appProps.load(new FileInputStream(appConfigPath));
        } catch (IOException e) {
            log.error("Can't read properties file.", e);
        }
    }

    public static String getProperty(String key) {
        if(appProps == null){
            loadPropertiesFile();
        }
        return appProps.getProperty(key);
    }
}
