package utils;

public class StringUtils {

    public static boolean isEmpty(String url) {
        return url == null || url.isEmpty();
    }
}
