package utils;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Optional;

@Slf4j
public class HttpConnection {

    public static Optional<HttpResponse<String>> getResponseFromURL(String url){
        if(StringUtils.isEmpty(url)){
            log.error("Missing URL with data. Check property file.");
            return Optional.empty();
        }
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .GET()
                .build();
        try {
            return Optional.ofNullable(client.send(request, HttpResponse.BodyHandlers.ofString()));
        } catch (IOException e) {
            log.error("Cant send http request.", e);
        } catch (InterruptedException e) {
            log.error("Sending http request operation has been interrupted", e);
        }
        return Optional.empty();
    }


}
