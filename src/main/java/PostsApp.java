import lombok.extern.slf4j.Slf4j;
import posts.PostsProcessor;
import posts.impl.JSONPostsFileWriter;
import posts.impl.JSONPostsHttpReader;

@Slf4j
public class PostsApp {

    public static void main(String[] args) {

        PostsProcessor postsProcessor = new PostsProcessor(new JSONPostsHttpReader(), new JSONPostsFileWriter());
        postsProcessor.start();

    }


}
